package test;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static com.codeborne.selenide.Selenide.*;

import my.cool.organization.pages.LoginPage;

public class SelenideTests {

    private LoginPage loginPage = new LoginPage();

    @BeforeAll
    public void setUp(){
        Configuration.baseUrl = "https://lmslite47vr.demo.mirapolis.ru/mira/";
        open();
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @Test
    public void loginHappyTest() {
        loginPage.doLogin ("fominaelena", "1P73BP4Z");
    }

    @Test
    public void resetPasswordTest() {
        loginPage.resetPassword("fominaelena");
        loginPage.getBackToHome();
    }

    @Test
    public void unPasswordTest() {
        loginPage.doLogin ("fominaelena", "1P73B");
        WebDriverRunner.getWebDriver().switchTo().alert().accept();
    }

    @Test
    public void unLoginSpaceTest() {
        loginPage.doLogin ("", "");
        WebDriverRunner.getWebDriver().switchTo().alert().accept();
    }

    @Test
    public void lowerCaseTest() {
        loginPage.doLogin("fominaelena", "1p73bp4z");
        WebDriverRunner.getWebDriver().switchTo().alert().accept();
    }

    @Test
    public void loginCapitalTest() {
        loginPage.doLogin ("FOMINAELENA","1P73BP4Z");
    }

    @Test
    public void loginSwapTest() {
        loginPage.doLogin ("1P73BP4Z", "fominaelena");
        WebDriverRunner.getWebDriver().switchTo().alert().accept();
    }

    @Disabled
    @AfterEach
    public void tearDown(){
    //  задержка закрытия браузера
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
